var args = arguments[0] || {};

$.header.showBackButton(true);
$.header.setBgColor(Alloy.CFG.general_color, '#FFF');
$.header.setTitle(L('to_comment'));
$.header.setWindow($.win);
$.header.setActionImage('/images/send.png', send);

var sendComment = require('send_comment');

function send() {
	if ($.author.value && $.message.value != '' && $.message.value != L('default_comment')) {
		new sendComment({
			product_id:args.id,
			author:$.author.value,
			message:$.message.value
		});
		Ti.UI.createAlertDialog({
			title:L('comments'),
			message:args.validation ? L('comment_sent_validation') : L('comment_sent'),
			ok:L('ok')
		}).show();
		Alloy.CFG.wins.pop();
		$.win.close({left:'100%'});
	} else {
		Ti.UI.createAlertDialog({
			title:L('error'),
			message:L('fill_data'),
			ok:L('ok')
		}).show();
	}
}

$.message.addEventListener('focus', function() {
	$.message.color = '#333';
	if ($.message.value == L('default_comment')) {
		$.message.value = '';
	}
});
$.message.addEventListener('blur', function() {
	if ($.message.value == '') {
		$.message.color = '#BBB';
		$.message.value = L('default_comment');
	}
});
