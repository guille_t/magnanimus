var current_win = null,
	f_callback = null,
	drop = false,
	f_dropdown = null;
	
exports.showBackButton = function(show) {
	if (show) {
		$.back.show();
	}
};
exports.showDrop = function(show, f_callback) {
	if (show) {
		$.drop.show();
		f_dropdown = f_callback;
	}
};
exports.setWindow = function(win) {
	current_win = win;
};
exports.setTitle = function(text) {
	$.title.text = text;
};
exports.setBgColor = function(color, color2) {
	$.header.backgroundColor = color;
	$.title.color = color2;
};
exports.setActionImage = function(image, func) {
	$.action.show();
	$.action_image.image = image;
	f_callback = func;
};
exports.drop = $.drop;

$.back.addEventListener('singletap', function() {
	Alloy.CFG.wins.pop();
	current_win.close({left:'100%'});
});

$.action.hide();

$.action.addEventListener('singletap', function() {
	if (f_callback) {
		f_callback();
	}
});
$.dropdown.addEventListener('singletap', function() {
	if (f_dropdown) {
		f_dropdown();
	}
});
