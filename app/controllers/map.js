var args = arguments[0] || {};

$.mapview.mapType = Alloy.Globals.Map.SATELLITE_TYPE;
$.mapview.animate = true;
$.mapview.userLocation = true;

$.mapview.addEventListener('click', function(e) {
	if (OS_IOS && e.clicksource == 'rightButton' || OS_ANDROID && e.clicksource == 'infoWindow' || e.clicksource == 'title' || e.clicksource == 'subtitle') {
		var win = Alloy.createController('view', e.annotation._data).getView();
		Alloy.CFG.wins.push(win);
		win.open({left:0});
	}
});

$.satellite.opacity = 1;

$.satellite.addEventListener('click', function() {
	$.mapview.mapType = Alloy.Globals.Map.SATELLITE_TYPE;
	this.opacity = 1;
	$.hybrid.opacity = $.normal.opacity = .6;
});
$.hybrid.addEventListener('click', function() {
	$.mapview.mapType = Alloy.Globals.Map.HYBRID_TYPE;
	this.opacity = 1;
	$.satellite.opacity = $.normal.opacity = .6;
});
$.normal.addEventListener('click', function() {
	$.mapview.mapType = Alloy.Globals.Map.NORMAL_TYPE;
	this.opacity = 1;
	$.hybrid.opacity = $.satellite.opacity = .6;
});

exports.myRemoveRoute = function(r) {
	$.mapview.removeRoute(r);
}

exports.myMapView = $.mapview;
exports.myWalking = $.walking;
exports.myDriving = $.driving;