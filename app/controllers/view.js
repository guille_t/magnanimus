var args = arguments[0] || {};
$.text.text = args.text;

$.header.showBackButton(true);
$.header.setBgColor(args.color, '#FFF');
$.header.setTitle(args.title);
$.header.setWindow($.win);
$.header.setActionImage('/images/config.png', change);

$.images.pagingControlColor = args.color;
$.table.separatorColor = args.color;

var color = args.color; // Para dentro de la función refresh

refresh(args);

function refresh(args) {
	if (args.checkin) {
		$.checkin.backgroundColor = '#DDD';
		$.checkin.enabled = false;
	} else {
		$.checkin.backgroundColor = color;
		$.checkin.enabled = true;
	}
	
	var votes = args.votes;
	var rating = args.rating;
	$.rating.text = rating + ' (' + (votes != 1 ? String.format(L('num_votes'), votes) : String.format(L('num_vote'), votes)) + ')';
	
	if (args.voted) {
		for (var i = 1; i <= args.voted; i ++) {
			eval('$.star' + i + '.children[0].image = "/images/star_black.png";');
		}
		for (var j = i + 1; j <= 5; j ++) {
			eval('$.star' + j + '.children[0].image = "/images/star.png";');
		}
	}
}

var submenu = false;

function change() {
	if (submenu) {
		submenu = false;
		if (OS_IOS) {
			$.submenu.animate({top:0});
		} else {
			$.submenu.top = 0;
		}
	} else {
		submenu = true;
		if (OS_IOS) {
			$.submenu.animate({top:(parseInt(Alloy.CFG.headerHeight) * 0.95) + 'dp'});
		} else {
			$.submenu.top = (parseInt(Alloy.CFG.headerHeight) * 0.95) + 'dp';
		}
	}
}

for (var j in Alloy.CFG.pins) {
	if (Alloy.CFG.pins[j].id == args.category_id) {
		var image = Alloy.CFG.pins[j].file;
	}
}

var a = Alloy.Globals.Map.createAnnotation({
	latitude:args.coords.lat,
    longitude:args.coords.lng,
    title:args.title,
    subtitle:args.title,
    //image:args.pin2,//args.pin,
    image:image,
    rightButton:OS_IOS ? Ti.UI.iPhone.SystemButton.INFO_LIGHT : null,
    _data:args
});

$.mapview.myMapView.addAnnotation(a);

$.mapview.myMapView.region = {
	latitude:args.coords.lat, longitude:args.coords.lng,
	latitudeDelta:0.01, longitudeDelta:0.01
};

$.win.addEventListener('open', function() {
	$.mapview.myWalking.hide();
	$.mapview.myDriving.hide();
});

var showing = 'view';

$.poi.addEventListener('singletap', function() {
	
	change();
	
	if (showing == 'view') {
		
		if (OS_IOS) {
			$.mapview.myMapView.animate({opacity:1});
		} else {
			$.mapview.myMapView.show();
		}
		
		showing = 'map';
		
		$.poi_image.image = '/images/list.png';
		$.poi_image.width = $.poi_image.height = '15dp';
		
	} else {
		
		if (OS_IOS) {
			$.mapview.myMapView.animate({opacity:0});
		} else {
			$.mapview.myMapView.hide();
		}
		
		
		$.poi_image.image = '/images/poi.png';
		$.poi_image.width = $.poi_image.height = '20dp';
		
		showing = 'view';
		
	}
	
});

var haveIt = false;
var query = Alloy.createCollection('favorite');
query.fetch({query:"SELECT * FROM favorite WHERE product_id = '" + args.id + "'"});

if (query.length > 0) {
	$.fav.opacity = .3;
	haveIt = true;
}

$.fav.addEventListener('singletap', function() {
	
	setTimeout(function() {
		change();
	}, 500);
	
	if (haveIt) {
		query.at(0).destroy();
		$.fav.opacity = 1;
		haveIt = false;
	} else {
		var favorite = Alloy.createModel('favorite', {
			product_id:args.id
		});
		favorite.save();
		$.fav.opacity = .3;
		query.fetch();
		haveIt = true;
		Ti.UI.createAlertDialog({
			title:L('favorites'),
			message:L('added_to_favorites'),
			ok:L('back')
		}).show();
	}
	
});

$.share.addEventListener('singletap', function() {
	change();
	showDialog();
});

$.banner.setBanner(args.category_id, $.win);

if (args.phone) {
	$.phone.text = L('phone') + ': ' + args.phone;
} else {
	$.row.remove($.phone);
}
if (args.web) {
	$.web.text = L('web') + ': ' + args.web;
} else {
	$.row.remove($.web);
}
if (args.email) {
	$.email.text = L('email') + ': ' + args.email;
} else {
	$.row.remove($.email);
}
if (args.facebook) {
	$.facebook.text = L('facebook') + ': ' + args.facebook;
} else {
	$.row.remove($.facebook);
}
if (args.twitter) {
	$.twitter.text = L('twitter') + ': ' + args.twitter;
} else {
	$.row.remove($.twitter);
}

$.phone.addEventListener('singletap', function() {
	Ti.Platform.openURL('tel:' + args.phone);
});
$.web.addEventListener('singletap', function() {
	var win = Alloy.createController('web', args.web).getView();
	Alloy.CFG.wins.push(win);
	win.open({left:0});
});
$.email.addEventListener('singletap', function() {
	var email = Ti.UI.createEmailDialog({
		toRecipients:[args.email],
		subject:L('contact')
	});
	email.open();
});
$.twitter.addEventListener('singletap', function() {
	var win = Alloy.createController('web', args.twitter).getView();
	Alloy.CFG.wins.push(win);
	win.open({left:0});
});
$.facebook.addEventListener('singletap', function() {
	var win = Alloy.createController('web', args.facebook).getView();
	Alloy.CFG.wins.push(win);
	win.open({left:0});
});

var size = '600,500';
var h = '250dp';
if (Ti.Platform.displayCaps.platformWidth > 720) {
	size = '1500,800';
	h = '400dp';
}

for (var i in args.images) {
	
	var view = Ti.UI.createView({
		width:Ti.UI.SIZE,
		height:Ti.UI.SIZE
	});
	var image = Ti.UI.createImageView({
		image:Alloy.CFG.url + args.images[i].url + ',fitCrop,' + size + '.' + args.images[i].ext,
		height:h,
		backgroundColor:'#FFF', // Puede venir un PNG con fondo transparente
		_i:i
	});
	if (args.images[i].title) {
		var label = Ti.UI.createLabel({
			zIndex:10,
			textAlign:'center',
			bottom:'20dp',
			width:'100%',
			height:Ti.UI.SIZE,
			backgroundColor:'#7000',
			color:'#FFF',
			font: {fontSize: Alloy.CFG.fontSize, fontFamily: Alloy.CFG.fontFamily},
			text: args.images[i].title,
		});
		view.add(label);
	}
	view.add(image);
	
	//images.push(Alloy.CFG.url + args.images[i].url + ',fitOutside,1000,1.' + args.images[i].ext);

	$.images.addView(view);
	
	// image.addEventListener('singletap', function(e) {
		// var gallery = Alloy.createController('gallery', {images:images, win:$.win});
		// gallery.showImage(e.source._i);
	// });

}

var ar = $.images.getViews();
setInterval(function(e) {
    if($.images.currentPage >= ar.length - 1) {
        $.images.scrollToView(0);
    } else {
    	$.images.scrollToView($.images.currentPage + 1);
    }
}, 3000);

var getComments = require('get_comments');
setTimeout(function() {
	new getComments(setComments, args.id);
}, 5000);

var validation = true;

function setComments(data) {
	$.num_comments.text = L('comments') + ' (' + data.comments.length + ')';
	validation = data.validation;
	var c = [];
	for (var i in data.comments) {
		var comment = Alloy.createController('comment', data.comments[i]).getView();
		c.push(comment);
	}
	$.table.appendRow(c);
}

$.comment.addEventListener('singletap', function() {
	var win = Alloy.createController('to_comment', {id:args.id, validation:validation}).getView();
	Alloy.CFG.wins.push(win);
	win.open({left:0});
	win.addEventListener('close', function() {
		for (var i = $.table.sections[0].rows.length - 1; i > 0; i --) {
			$.table.deleteRow($.table.sections[0].rows[i]);
		}
		setTimeout(function() {
			new getComments(setComments, args.id);
		}, 1000);
	});
});

$.star1.addEventListener('singletap', function() {
	$.star1.children[0].image = '/images/star_black.png';
	$.star2.children[0].image = '/images/star.png';
	$.star3.children[0].image = '/images/star.png';
	$.star4.children[0].image = '/images/star.png';
	$.star5.children[0].image = '/images/star.png';
	vote(1);
});
$.star2.addEventListener('singletap', function() {
	$.star1.children[0].image = '/images/star_black.png';
	$.star2.children[0].image = '/images/star_black.png';
	$.star3.children[0].image = '/images/star.png';
	$.star4.children[0].image = '/images/star.png';
	$.star5.children[0].image = '/images/star.png';
	vote(2);
});
$.star3.addEventListener('singletap', function() {
	$.star1.children[0].image = '/images/star_black.png';
	$.star2.children[0].image = '/images/star_black.png';
	$.star3.children[0].image = '/images/star_black.png';
	$.star4.children[0].image = '/images/star.png';
	$.star5.children[0].image = '/images/star.png';
	vote(3);
});
$.star4.addEventListener('singletap', function() {
	$.star1.children[0].image = '/images/star_black.png';
	$.star2.children[0].image = '/images/star_black.png';
	$.star3.children[0].image = '/images/star_black.png';
	$.star4.children[0].image = '/images/star_black.png';
	$.star5.children[0].image = '/images/star.png';
	vote(4);
});
$.star5.addEventListener('singletap', function() {
	$.star1.children[0].image = '/images/star_black.png';
	$.star2.children[0].image = '/images/star_black.png';
	$.star3.children[0].image = '/images/star_black.png';
	$.star4.children[0].image = '/images/star_black.png';
	$.star5.children[0].image = '/images/star_black.png';
	vote(5);
});

function vote(v) {
	var client = Ti.Network.createHTTPClient({
		onload: function() {
			Ti.API.info('VOTE: ' + this.responseText);
			var result = JSON.parse(this.responseText);
			if (result.status == 'ok') {
				Ti.API.info('Voted');
				votes = result.data.votes;
				rating = result.data.rating;
				$.rating.text = rating + ' (' + (votes != 1 ? String.format(L('num_votes'), votes) : String.format(L('num_vote'), votes)) + ')';
			}
		},
		onerror: function(e) {
			Ti.API.info(e);
		},
		timeout: 10000
	});
	client.open('POST', Alloy.CFG.url + 'votes/vote');
	client.send({
		product_id:args.id,
		device_token:Ti.App.Properties.getString('device_token', null),
		vote:v
	});
}

$.checkin.addEventListener('click', function() {
	Ti.Geolocation.getCurrentPosition(function(e) {
		if (!e.coords) {
			Ti.UI.createAlertDialog({
				title:L('checkin'),
				message:L('no_gps'),
				ok:L('ok')
			}).show();
			return;
		}
		var client = Ti.Network.createHTTPClient({
			onload: function() {
				Ti.API.info('CHECKIN: ' + this.responseText);
				var result = JSON.parse(this.responseText);
				if (result.status == 'ok') {
					var message = L('checkin_ok');
					$.checkin.backgroundColor = '#DDD';
					$.checkin.enabled = false;
				} else {
					var message = L('checkin_ko');
				}
				Ti.UI.createAlertDialog({
					title:L('checkin'),
					message:message,
					ok:L('ok')
				}).show();
			},
			onerror: function(e) {
				Ti.API.info(e);
			},
			timeout: 10000
		});
		client.open('POST', Alloy.CFG.url + 'products/checkin');
		client.send({
			product_id:args.id,
			device_token:Ti.App.Properties.getString('device_token', null),
			latitude:e.coords.latitude,
			longitude:e.coords.longitude
		});
	});
});

var client = Ti.Network.createHTTPClient({
	onload: function() {
		Ti.API.info('REFRESH: ' + this.responseText);
		var result = JSON.parse(this.responseText);
		if (result.status == 'ok') {
			refresh(result.data);
		}
	},
	onerror: function(e) {
		Ti.API.info(e);
	},
	timeout: 10000
});
client.open('POST', Alloy.CFG.url + 'products/refresh');
client.send({
	product_id:args.id,
	device_token:Ti.App.Properties.getString('device_token', null)
});

function showDialog() {
	var dialog = Ti.UI.createAlertDialog({
		title:L('share'),
		buttonNames:[L('email'), 'Facebook', 'Twitter', L('cancel')],
		cancel:3
	});
	dialog.show();
	dialog.addEventListener('click', function(e) {
		switch (e.index) {
			case 0:
				sendEmail();
				break;
			case 1:
				shareFacebook();
				break;
			case 2:
				shareTwitter();
				break;
		}
	});
}

function sendEmail() {
	var email = Ti.UI.createEmailDialog({
		subject:args.title,
		html:true,
		toRecipients:null,
		messageBody:$.text.text.replace(/\n/g, '<br />') + '<br /><br />' +
			($.phone.text ? $.phone.text + '<br /><br />' : '') +
			($.web.text ? $.web.text + '<br /><br />' : '') +
			($.email.text ? $.email.text + '<br /><br />' : '') +
			($.facebook.text ? $.facebook.text + '<br /><br />' : '') +
			($.twitter.text ? $.twitter.text + '<br /><br />' : '')
	});
	var attached = 0;
	for (var i in args.images) {
		var url = Alloy.CFG.url + args.images[i].url + '.' + args.images[i].ext;
		var client = Ti.Network.createHTTPClient({
			_path:Ti.Filesystem.applicationCacheDirectory + args.images[i].id + '.' + args.images[i].ext,
			onload:function(e) {
				var file = Ti.Filesystem.getFile(this._path);
				file.write(this.responseData);
				email.addAttachment(file);
				attached ++;
				if (attached == args.images.length) {
					email.open();
				}
			},
			timeout:10000
		});
		client.open('GET', url);
		client.send();
	}
	if (args.images.length == 0) {
		email.open();
	}
}

var shareTitle = args.title;
var shareText = '';
var shareURL = args.url;

function shareFacebook() {
	if (OS_IOS) {
		var Social = require('dk.napp.social');
		if(Social.isFacebookSupported()){ //min iOS6 required
			Social.facebook({
				text:shareTitle + ' ' + shareText,
				url:shareURL
			});
	    } else {
			oldMode('f');
	    }
	} else {
		oldMode('f');
	}
}

function shareTwitter() {
	if (OS_IOS) {
		var Social = require('dk.napp.social');
		if(Social.isTwitterSupported()){ //min iOS5 required
			Social.twitter({
				text:shareTitle,
				url:shareURL
			});
		} else {
			oldMode('t');
		}
	} else {
		oldMode('t');
	}
}

function oldMode(type) {
	var win = Ti.UI.createWindow({backgroundColor:'#333'});
	if (type == 't') {
		win.add(Ti.UI.createWebView({top:'50dp', url:'http://twitter.com/share?text=' + shareTitle + ' ' + shareURL}));
	} else {
		win.add(Ti.UI.createWebView({top:'50dp', url:'http://facebook.com/sharer.php?u=' + shareURL}));
	}
	var close = Ti.UI.createButton({title:'Cerrar', left:'10dp', top:'15dp', backgroundImage:'none', color:'#FFF'});
	win.add(close);
	win.open();
	close.addEventListener('click', function() {
		win.close();
	});
}
