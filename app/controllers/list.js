var args = arguments[0] || {};

$.header.showBackButton(true);
$.header.setBgColor(args.color, '#FFF');
$.header.setTitle(args.title);
$.header.setWindow($.win);

var route;
var points = [];

var dropped = false;
var down = Ti.UI.create2DMatrix();
down = down.rotate(0);
var up = Ti.UI.create2DMatrix();
up = up.rotate(180);

function dropdown() {
	if (dropped) {
		dropped = false;
		$.dropdown.animate({top:'-500dp'});
		$.header.drop.animate({transform:down});
	} else {
		$.dropdown.animate({top:Alloy.CFG.headerHeight});
		dropped = true;
		$.header.drop.animate({transform:up});
	}
}

var type = 'list';
function change() {
	if (type == 'list') {
		$.header.setActionImage('/images/list.png', change);
		type = 'geo';
		if (OS_IOS) {
			$.mapview.myMapView.animate({opacity:1});
		} else {
			$.mapview.myMapView.show();
		}
	} else {
		$.header.setActionImage('/images/geo.png', change);
		type = 'list';
		if (OS_IOS) {
			$.mapview.myMapView.animate({opacity:0});
		} else {
			$.mapview.myMapView.hide();
		}
	}
}

$.loading.show();

var getCategories = require('get_categories');
new getCategories(setCategories);

function setCategories(buttons) {
	for (var i in buttons) {
		var row = Ti.UI.createTableViewRow({
			backgroundColor:'transparent',
			title:buttons[i].title,
			color:'#FFF',
			font: {
				fontSize:Alloy.CFG.fontSizeSmall,
				fontFamily:Alloy.CFG.fontFamily
			},
			_data:buttons[i]
		});
		$.dropdown.appendRow(row);
	}
}

$.dropdown.addEventListener('click', function(e) {
	dropdown();
	points = [];
	$.table.data = [];
	for (var i = $.mapview.myMapView.annotations.length - 1; i >= 0; i--) {
		$.mapview.myMapView.removeAnnotation($.mapview.myMapView.annotations[i]);
	}
	for (var i = $.mapview.myMapView.annotations.length - 1; i >= 0; i--) { // Lo pongo dos veces porque a veces no quita todos los puntos
		$.mapview.myMapView.removeAnnotation($.mapview.myMapView.annotations[i]);
	}
	if (route) {
		$.mapview.myMapView.removeRoute(route);
	}
	args = e.row._data;
	$.loading.show();
	new getData(setData, e.row._data);
});

var getData = require('get_products');
new getData(setData, args);

function setData(data, args) {
	
	$.header.showBackButton(true);
	$.header.setBgColor(args.color, '#FFF');
	$.header.setTitle(args.title);
	$.header.setWindow($.win);
	$.header.setActionImage('/images/geo.png', change);
	$.header.showDrop(true, dropdown);
	
	if (data.data == null) {
		$.loading.hide();
		return;
	}

	var rows = [];
	var sum_lat = 0,
		sum_lng = 0;
		
	for (var i in data.data) {
		//data.data[i].color = args.color;
		var row = Alloy.createController('row', data.data[i]).getView();
		row._data = data.data[i];
		rows.push(row);
		
		for (var j in Alloy.CFG.pins) {
			if (Alloy.CFG.pins[j].id == data.data[i].category_id) {
				var image = Alloy.CFG.pins[j].file;
			}
		}

		var a = Alloy.Globals.Map.createAnnotation({
			latitude:data.data[i].coords.lat,
		    longitude:data.data[i].coords.lng,
		    title:data.data[i].title,
		    subtitle:args.title,
		    //image:data.data[i].pin,//args.pin,
		    image:image,
			//leftView:Ti.UI.createImageView({image:image, width:'32dp', height:'32dp'}),
		    rightButton:OS_IOS ? Ti.UI.iPhone.SystemButton.INFO_LIGHT : null,
		    _data:data.data[i]
		});
		
		$.mapview.myMapView.addAnnotation(a);
		sum_lat += parseFloat(data.data[i].coords.lat);
		sum_lng += parseFloat(data.data[i].coords.lng);
		points.push({latitude:data.data[i].coords.lat, longitude:data.data[i].coords.lng});
	}
	
	$.mapview.myMapView.region = {
		latitude:sum_lat / data.data.length, longitude:sum_lng / data.data.length,
		latitudeDelta:data.delta.lat, longitudeDelta:data.delta.lng
	};
	
	// Mostrando la ruta sólo para una categoría en concreto
	var mode = 'walking';
	if (args.id == '52d5217d-9f30-40cf-a57d-589fbca5e1a6') {
		loadRoute(points, mode);
	} else {
		$.mapview.myWalking.hide();
		$.mapview.myDriving.hide();
	}
	
	$.table.appendRow(rows);
	
	$.loading.hide();
	
}

$.table.addEventListener('click', function(e) {
	
	e.row._view.animate({backgroundColor:e.row._data.color}, function() {
		e.row._view.backgroundColor = '#CCC';
	});
	
	var win = Alloy.createController('view', e.row._data).getView();
	Alloy.CFG.wins.push(win);
	win.open({left:0});
	
});

function loadRoute(points, mode) {
	if (args.general) {
		$.mapview.myWalking.hide();
		$.mapview.myDriving.hide();
		return;
	} else {
		if ($.mapview.myWalking.visible === false) {
			$.mapview.myWalking.show();
			$.mapview.myDriving.show();
		}
	}
	var waypoints = [];
	for (var i = 1; i < points.length - 1; i ++) {
		waypoints.push(points[i].latitude + ',' + points[i].longitude);
	}
	if (points.length == 0) {
		return;
	}
	var url = 'http://maps.googleapis.com/maps/api/directions/json?' +
		'origin=' + points[0].latitude + ',' + points[0].longitude +
		'&destination=' + points[points.length - 1].latitude + ',' + points[points.length - 1].longitude +
		'&waypoints=' + waypoints.join('|') + // Separado por |
		'&sensor=false' +
		'&mode=' + mode;
	
	var client = Ti.Network.createHTTPClient({
		onload: function() {
			var result = JSON.parse(this.responseText);
			
			var steps = [];
			
			for (var i in result.routes[0].legs) {
				for (var j in result.routes[0].legs[i].steps) {
					var coords = result.routes[0].legs[i].steps[j].start_location;
					steps.push({latitude:coords.lat, longitude:coords.lng});
				}
			}
			var coords = result.routes[0].legs[i].steps[j].end_location;
			steps.push({latitude:coords.lat, longitude:coords.lng});
			
			route = Alloy.Globals.Map.createRoute({
				points:steps,
				color:args.color,
				width:4
			});
			$.mapview.myMapView.addRoute(route);
			
		},
		onerror: function(e) {
			alert(e);
		},
		timeout: 10000
	});
	client.open('GET', url);
	client.send();
}

$.mapview.myWalking.opacity = 1;

$.mapview.myWalking.addEventListener('click', function() {
	$.mapview.myMapView.removeRoute(route);
	loadRoute(points, 'walking');
	this.opacity = 1;
	$.mapview.myDriving.opacity = .6;
});
$.mapview.myDriving.addEventListener('click', function() {
	$.mapview.myMapView.removeRoute(route);
	loadRoute(points, 'driving');
	this.opacity = 1;
	$.mapview.myWalking.opacity = .6;
});

$.banner.setBanner(args.id, $.win);