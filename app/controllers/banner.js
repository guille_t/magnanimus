var getBanner = require('get_banner');

exports.setBanner = function(category_id, win) {
	new getBanner(setBanner, category_id);
	var interval = setInterval(function() {
		new getBanner(setBanner, category_id);
	}, 8000);
	win.addEventListener('close', function() {
		clearInterval(interval);
	});
};

function setBanner(data) {
	if (data) {
		$.image._data = data;
		$.image.image = data.image;
	} else {
		$.image.removeEventListener('singletap', function() {
			//alert('canceled');
		});
	}
}

$.image.addEventListener('singletap', function() {
	if (this._data.product) {
		var win = Alloy.createController('view', this._data.product).getView();
		Alloy.CFG.wins.push(win);
		win.open({left:0});
	} else if (this._data.url){
		var win = Alloy.createController('web', this._data.url).getView();
		Alloy.CFG.wins.push(win);
		win.open({left:0});
	} else {
		Ti.API.info('No tiene ni producto ni enlace');
	}
});

$.club.addEventListener('singletap', function() {
	var win = Alloy.createController('web', 'http://mayogarcia.com/contacto/').getView();
	Alloy.CFG.wins.push(win);
	win.open({left:0});
});
