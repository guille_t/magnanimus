var args = arguments[0] || {};

$.header.showBackButton(true);
$.header.setBgColor(Alloy.CFG.general_color, '#FFF');
$.header.setTitle(args);
$.header.setWindow($.win);

$.web.url = args;

setTimeout(function() {
	loaded();
}, 3000);

function loaded() {
	$.loading.hide();
	$.reload.show();

	if (!$.web.canGoBack()) {
		$.back.enabled = false;
	} else {
		$.back.enabled = true;
	}
	if (!$.web.canGoForward()) {
		$.fwd.enabled = false;
	} else {
		$.fwd.enabled = true;
	}
}

$.web.addEventListener('load', function() {

	loaded();

});

$.back.addEventListener('click', function() {
	if ($.web.canGoBack()) {
		$.web.goBack();
	}
});

$.fwd.addEventListener('click', function() {
	if ($.web.canGoForward()) {
		$.web.goForward();
	}
});

$.reload.addEventListener('click', function() {
	$.web.reload();
});

$.web.addEventListener('beforeload', function() {
	$.loading.show();
	$.reload.hide();
	setTimeout(function() {
		loaded();
	}, 3000);
});

$.open.addEventListener('click', function() {
	Ti.Platform.openURL($.web.url);
});
