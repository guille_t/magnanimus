var args = arguments[0] || {};

$.title.text = args.title;
$.image.image = args.image;

$.row._view = $.view;

$.indicator.backgroundColor = args.color;