var args = arguments[0] || {};

$.header.showBackButton(true);
$.header.setBgColor(Alloy.CFG.general_color, '#FFF');
$.header.setTitle(L('favorites'));
$.header.setWindow($.win);
$.header.setActionImage('/images/geo.png', change);

var route;
var points = [];

var type = 'list';
function change() {
	if (type == 'list') {
		$.header.setActionImage('/images/list.png', change);
		type = 'geo';
		if (OS_IOS) {
			$.mapview.myMapView.animate({opacity:1});
		} else {
			$.mapview.myMapView.show();
		}
	} else {
		$.header.setActionImage('/images/geo.png', change);
		type = 'list';
		if (OS_IOS) {
			$.mapview.myMapView.animate({opacity:0});
		} else {
			$.mapview.myMapView.hide();
		}
	}
}

function noFavorites() {
	var back = Ti.UI.createAlertDialog({
		title:L('error'),
		message:L('no_favorites'),
		ok:L('back')
	});
	back.show();
	back.addEventListener('click', function() {
		Alloy.CFG.wins.pop();
		$.win.close({left:'100%'});
	});
}

$.loading.show();

var query = Alloy.createCollection('favorite');
query.fetch({query:"SELECT * FROM favorite"});

$.win.addEventListener('open', function() {
	if (query.length == 0) {
		noFavorites();
	}
});

if (query.length > 0) {
	var ids = [];
	for (var i = 0; i < query.length; i ++) {
		ids.push(query.at(i).get('product_id'));
	}
	
	var getFavorites = require('get_favorites');
	new getFavorites(setData, ids);
}

function setData(data) {
	
	if (data.data == null) {
		$.loading.hide();
		return;
	}

	var rows = [];
	var sum_lat = 0,
		sum_lng = 0;
	
	for (var i in data.data) {
		//data.data[i].color = args.color;
		var row = Alloy.createController('row', data.data[i]).getView();
		row._data = data.data[i];
		rows.push(row);
		
		var a = Alloy.Globals.Map.createAnnotation({
			latitude:data.data[i].coords.lat,
		    longitude:data.data[i].coords.lng,
		    title:data.data[i].title,
		    //subtitle:args.title,
		    image:data.data[i].pin,//args.pin,
		    rightButton:OS_IOS ? Ti.UI.iPhone.SystemButton.INFO_LIGHT : null,
		    _data:data.data[i]
		});
		
		$.mapview.myMapView.addAnnotation(a);
		sum_lat += parseFloat(data.data[i].coords.lat);
		sum_lng += parseFloat(data.data[i].coords.lng);
		points.push({latitude:data.data[i].coords.lat, longitude:data.data[i].coords.lng});
	}
	
	$.mapview.myMapView.region = {
		latitude:sum_lat / data.data.length, longitude:sum_lng / data.data.length,
		latitudeDelta:data.delta.lat, longitudeDelta:data.delta.lng
	};
	
	// Obtener la ruta de Google Maps a partir de un listado de puntos
	var mode = 'walking';
	//loadRoute(points, mode);
	
	$.table.appendRow(rows);
	
	$.loading.hide();
	
}

var win = null;

$.table.addEventListener('click', function(e) {
	
	e.row._view.animate({backgroundColor:e.row._data.color}, function() {
		e.row._view.backgroundColor = '#CCC';
	});
	
	win = Alloy.createController('view', e.row._data).getView();
	Alloy.CFG.wins.push(win);
	win.open({left:0});
	
	win.addEventListener('close', function() {
		
		$.table.data = [];
		
		$.loading.show();
	
		query.fetch({query:"SELECT * FROM favorite"});
		
		if (query.length == 0) {
			noFavorites();
		}
		
		ids = [];
		for (var i = 0; i < query.length; i ++) {
			ids.push(query.at(i).get('product_id'));
		}
		
		new getFavorites(setData, ids);
	});
	
});

function loadRoute(points, mode) {
	if (args.general) {
		$.mapview.myWalking.hide();
		$.mapview.myDriving.hide();
		return;
	} else {
		if ($.mapview.myWalking.visible === false) {
			$.mapview.myWalking.show();
			$.mapview.myDriving.show();
		}
	}
	var waypoints = [];
	for (var i = 1; i < points.length - 1; i ++) {
		waypoints.push(points[i].latitude + ',' + points[i].longitude);
	}
	if (points.length == 0) {
		return;
	}
	var url = 'http://maps.googleapis.com/maps/api/directions/json?' +
		'origin=' + points[0].latitude + ',' + points[0].longitude +
		'&destination=' + points[points.length - 1].latitude + ',' + points[points.length - 1].longitude +
		'&waypoints=' + waypoints.join('|') + // Separado por |
		'&sensor=false' +
		'&mode=' + mode;
	
	var client = Ti.Network.createHTTPClient({
		onload: function() {
			var result = JSON.parse(this.responseText);
			
			var steps = [];
			
			for (var i in result.routes[0].legs) {
				for (var j in result.routes[0].legs[i].steps) {
					var coords = result.routes[0].legs[i].steps[j].start_location;
					steps.push({latitude:coords.lat, longitude:coords.lng});
				}
			}
			var coords = result.routes[0].legs[i].steps[j].end_location;
			steps.push({latitude:coords.lat, longitude:coords.lng});
			
			route = Alloy.Globals.Map.createRoute({
				points:steps,
				color:args.color
			});
			$.mapview.myMapView.addRoute(route);
			
		},
		onerror: function(e) {
			alert(e);
		},
		timeout: 10000
	});
	client.open('GET', url);
	client.send();
}

$.win.addEventListener('open', function() {
	$.mapview.myWalking.hide();
	$.mapview.myDriving.hide();
});

$.banner.setBanner(args.id, $.win);