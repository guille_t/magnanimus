
setTimeout(function() {
	$.win.open();
}, 1500);

/*
 * Notifications
 */
if (OS_IOS) {
	var Cloud = require('cloud_ios');
} else {
	var Cloud = require('cloud_android');
}
new Cloud(function(notification) {
	// do nothing
	Ti.UI.iPhone.appBadge = 0;
}, $.win);
Ti.UI.iPhone.appBadge = 0;
$.win.addEventListener('resume', function() {
	Ti.UI.iPhone.appBadge = 0;
});

$.header.setActionImage('/images/fav.png', showFavs);

Alloy.CFG.wins = []; // Ventanas abiertas (para header.js)

var getImages = require('get_images');
new getImages(setImages);

function setImages(images) {
	for (var i in images) {
		if (Ti.Platform.displayCaps.platformWidth > 720) {
			var img = images[i].image_tablet;
		} else {
			var img = images[i].image;
		}
		var img = Ti.UI.createImageView({image:img});
		$.scrollable.addView(img);
	}
	var ar = $.scrollable.getViews();
	setInterval(function(e) {
	    if($.scrollable.currentPage >= ar.length - 1) {
	        $.scrollable.scrollToView(0);
	    } else {
	    	$.scrollable.scrollToView($.scrollable.currentPage + 1);
	    }
	}, 5000);
}

var getCategories = require('get_categories');
new getCategories(setCategories);

function setCategories(buttons) {
	for (var i in buttons) {
		var row = Alloy.createController('button', buttons[i]).getView();
		row._data = buttons[i];
		$.table.appendRow(row);
		if (buttons[i].general) {
			Alloy.CFG.general_color = buttons[i].color;
			$.header.setBgColor(buttons[i].color, '#FFF');
			$.scrollable.pagingControlColor = buttons[i].color;
		}
		var url = Alloy.CFG.url + 'img/Category/' + buttons[i].pin2;
		Alloy.Globals.saveImage(url, buttons[i].id);
	}
}

$.table.addEventListener('click', function(e) {
	e.row._view.animate({backgroundColor:e.row._data.color}, function() {
		e.row._view.backgroundColor = '#CCC';
	});
	
	var win = Alloy.createController('list', e.row._data).getView();
	Alloy.CFG.wins.push(win);
	win.open({left:0});
});

$.banner.setBanner(null, $.win);

function showFavs() {
	var win = Alloy.createController('favorites').getView();
	Alloy.CFG.wins.push(win);
	win.open({left:0});
}