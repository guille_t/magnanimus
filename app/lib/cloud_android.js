
module.exports = function(f_callback, win) {
	
	var CloudPush = require('ti.cloudpush');
	//CloudPush.debug = true;
	CloudPush.enabled = true;
	CloudPush.showTrayNotificationsWhenFocused = true;
	CloudPush.focusAppOnPush = false;
	 
	var deviceToken;
	var Cloud = require('ti.cloud');
	//Cloud.debug = true;
	
	var password = 'DKJ3aD8JmV5Nbcd73';
	 
	CloudPush.retrieveDeviceToken({
		success: function deviceTokenSuccess(e) {
			deviceToken = e.deviceToken;
			Ti.App.Properties.setString("device_token", deviceToken);
			registerUser(); // TODO sólo quiero por ahora el device token
		},
		error: function deviceTokenError(e) {
			//alert('Failed to register for push! ' + e.error);
		}
	});
	
	function registerUser() {
		Cloud.Users.create({
		    username: deviceToken,
		    password: password,
		    password_confirmation: password,
		    first_name: "Firstname",
		    last_name: "Lastname"
		}, function (e) {
		    if (e.success) {
		    	Ti.API.info('registerUser OK');
		    } else {
		    	Ti.API.info('error registering user');
		    }
		    login();
		});
	}
	 
	function login(e){
		//Create a Default User in Cloud Console, and login
		Cloud.Users.login({
			login: deviceToken,
			password: password
			}, function (e) {
			if (e.success) {
				//alert("login success");
				subscribe();
			} else {
				//alert('Error: ' +((e.error && e.message) || JSON.stringify(e)));
			}
		});
	}
	
	function subscribe(){
		Cloud.PushNotifications.subscribe({
			channel: 'notifications',
			device_token: deviceToken,
			//type: 'android'
			type: 'gcm'
		}, function (e){
			if (e.success) {
				//alert('Subscribed for Push Notification!');
				send();
			}else{
				//alert('Error:' +((e.error && e.message) || JSON.stringify(e)));
			}
		});
	}
	
	CloudPush.addEventListener('callback', function (evt) {
		//alert('1');
		if (win.isOpened) {
			f_callback(evt);
		} else {
			win.addEventListener('open', function() {
				f_callback(evt);
			});
		}
		//alert(evt);
		//alert(evt.payload);
	});
	 
	CloudPush.addEventListener('trayClickLaunchedApp', function (evt) { // Cuando lo llama, también llama a callback
		//f_callback(evt);
		//alert('2');
		//alert(evt);
		//Ti.API.info('Tray Click Launched App (app was not running)');
		//alert('Tray Click Launched App (app was not running');
	});
	 
	CloudPush.addEventListener('trayClickFocusedApp', function (evt) { // No parece que lo llame nunca
		f_callback(evt);
		//alert('3');
		//alert(evt);
		//Ti.API.info('Tray Click Focused App (app was already running)');
		//alert('Tray Click Focused App (app was already running)');
	});
	
	/*
	function send() {
		Cloud.PushNotifications.notify({
		    channel: 'notifications',
		    payload: 'This is a test.',
		    friends: true
		}, function (e) {
		    if (e.success) {
		        alert('Push notification sent');
		    } else {
		        alert('Error:\n' +
		            ((e.error && e.message) || JSON.stringify(e)));
		    }
		});
	}
	*/
};
