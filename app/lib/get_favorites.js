
module.exports = function(f_callback, ids) {
	var category_id = category_id || '';
	var client = Ti.Network.createHTTPClient({
		onload: function() {
			Ti.API.info('FAVORITES: ' + this.responseText);
			var result = JSON.parse(this.responseText);
			if (result.status == 'ok') {
				f_callback(result.data);
			} else {
				f_callback(null);
			}
		},
		onerror: function(e) {
			Ti.API.info(e);
			f_callback(null);
		},
		timeout: 10000
	});
	client.open('POST', Alloy.CFG.url + 'products/favorites');
	client.send({
		ids:ids.join(','),
		device_token:Ti.App.Properties.getString('device_token'),
		lang:Ti.Locale.currentLanguage
	});
};