
module.exports = function(f_callback, args) {
	/*
	if (Ti.App.Properties.getString('subcats_' + id, null)) {
		var result = JSON.parse(Ti.App.Properties.getString('subcats_' + id, null));
		f_callback(result.data);
		return;
	}
	*/
	var client = Ti.Network.createHTTPClient({
		onload: function() {
			Ti.API.info('PRODUCTS: ' + this.responseText);
			var result = JSON.parse(this.responseText);
			if (result.status == 'ok') {
				f_callback(result.data, args);
			} else {
				f_callback(null);
			}
		},
		onerror: function(e) {
			Ti.API.info(e);
			f_callback(null);
		},
		timeout: 10000
	});

	client.open('POST', Alloy.CFG.url + 'products/json/' + args.id);
	client.send({
		device_token:Ti.App.Properties.getString('device_token'),
		lang:Ti.Locale.currentLanguage
	});
};