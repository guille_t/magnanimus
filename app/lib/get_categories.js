
module.exports = function(f_callback) {
	/*
	if (Ti.App.Properties.getString('subcats_' + id, null)) {
		var result = JSON.parse(Ti.App.Properties.getString('subcats_' + id, null));
		f_callback(result.data);
		return;
	}
	*/
	var client = Ti.Network.createHTTPClient({
		onload: function() {
			var result = JSON.parse(this.responseText);
			Ti.API.info('CATEGORIES: ' + this.responseText);
			if (result.status == 'ok') {
				f_callback(result.data);
			} else {
				Ti.API.info(this.responseText);
				f_callback(null);
			}
		},
		onerror: function(e) {
			Ti.API.info(e);
			f_callback(null);
		},
		timeout: 10000
	});
	client.open('POST', Alloy.CFG.url + 'categories/json');
	client.send({
		lang:Ti.Locale.currentLanguage
	});
};