
module.exports = function(f_callback, id) {

	var client = Ti.Network.createHTTPClient({
		onload: function() {
			Ti.API.info('COMMENTS: ' + this.responseText);
			var result = JSON.parse(this.responseText);
			if (result.status == 'ok') {
				f_callback(result.data);
			} else {
				Ti.API.info(this.responseText);
				f_callback(null);
			}
		},
		onerror: function(e) {
			Ti.API.info(e);
			f_callback(null);
		},
		timeout: 10000
	});
	client.open('POST', Alloy.CFG.url + 'comments/json');
	client.send({
		product_id:id
	});
};