
module.exports = function(f_callback) {
	var client = Ti.Network.createHTTPClient({
		onload: function() {
			var result = JSON.parse(this.responseText);
			Ti.API.info('IMAGES: ' + this.responseText);
			if (result.status == 'ok') {
				f_callback(result.data);
			} else {
				Ti.API.info(this.responseText);
				f_callback(null);
			}
		},
		onerror: function(e) {
			Ti.API.info(e);
			f_callback(null);
		},
		timeout: 10000
	});
	client.open('GET', Alloy.CFG.url + 'slides/json');
	client.send();
};