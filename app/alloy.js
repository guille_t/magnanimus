// The contents of this file will be executed before any of
// your view controllers are ever executed, including the index.
// You have access to all functionality on the `Alloy` namespace.
//
// This is a great place to do any initialization for your app
// or create any global variables/functions that you'd like to
// make available throughout your app. You can easily make things
// accessible globally by attaching them to the `Alloy.Globals`
// object. For example:
//
// Alloy.Globals.someGlobalFunction = function(){};

Alloy.Globals.Map = require('ti.map');

var isTablet = Ti.Platform.osname === 'ipad' || (Ti.Platform.osname === 'android' && (Ti.Platform.displayCaps.platformWidth > 899 || Ti.Platform.displayCaps.platformHeight > 899));

if (isTablet) {
	Alloy.CFG.fontSize = '20dp';
	Alloy.CFG.fontSizeSmall = '15dp';
	Alloy.CFG.headerHeight = '70dp';
}

Alloy.CFG.pins = [];

Alloy.Globals.saveImage = function(url, name) {
	var file = Ti.Filesystem.getFile(Ti.Filesystem.applicationDataDirectory + name + '.jpg');
	if (!file.exists()) {
		var client = Ti.Network.createHTTPClient({
			timeout:15000,
			onload:function() {
				file.write(client.responseData);
				//Alloy.CFG.pins.push(file);
				Ti.API.info('pins ' + Alloy.CFG.pins);
				Alloy.CFG.pins.push({id:name, file:file.nativePath});
			},
			onerror:function(e) {
			},
			timeout:10000
		});
		client.open('GET', url);
		client.send();
	} else {
		Alloy.CFG.pins.push({id:name, file:file.nativePath});
	}
};
